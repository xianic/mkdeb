all:

install:
	[ -n "$(DESTDIR)" ] && mkdir -p $(DESTDIR)
	cp -Rt "$(DESTDIR)" src/*

BUILD_NUMBER ?= 0
PROJECT_NAME := mkdeb
PROJECT_VERSION := 0.1.$(BUILD_NUMBER)
PACKAGE_VERSION = 1xianic1~$(DSC_CODENAME)1
MKCHANGELOG := src/usr/share/mkdeb/mkchangelog
include src/usr/share/mkdeb/mkdeb.mk

clean:
	rm -rf mkdeb*
