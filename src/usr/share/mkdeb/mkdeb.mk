DSC_CODENAME ?= $(shell lsb_release -cs)
DISTNAME := $(PROJECT_NAME)-$(PROJECT_VERSION)
TARNAME := $(PROJECT_NAME)_$(PROJECT_VERSION).orig.tar.xz
MKCHANGELOG ?= /usr/share/mkdeb/mkchangelog
PACKAGE_VERSION ?= 1
FULL_VERSION := $(PROJECT_VERSION)-$(PACKAGE_VERSION)
DSC_FILE := $(PROJECT_NAME)_$(FULL_VERSION).dsc
CHANGELOG := $(DISTNAME)/debian/changelog
.PHONY: dist dsc

$(TARNAME): .git/index
	git archive --prefix "$(DISTNAME)/" HEAD | xz > "$@"

dist: $(TARNAME)

$(CHANGELOG): $(TARNAME)
	rm -rf $(DISTNAME) && tar -xJf $(TARNAME)
	"$(MKCHANGELOG)" . "$(dir $(CHANGELOG))" "$(FULL_VERSION)" -c "$(DSC_CODENAME)"

$(DSC_FILE): $(CHANGELOG)
	cd $(DISTNAME) && debuild -S

dsc: $(DSC_FILE)

deb: dsc
	(cd $(DISTNAME) && dpkg-buildpackage --build=any,all)

debclean:
	rm -rf $(PROJECT_NAME)[-_]$(PROJECT_VERSION)*
