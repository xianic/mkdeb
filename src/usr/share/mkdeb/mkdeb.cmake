set(distname ${PROJECT_NAME}-${PROJECT_VERSION})
set(tarname ${PROJECT_NAME}_${PROJECT_VERSION}.orig.tar.xz)
if(NOT DEFINED MKCHANGELOG)
    set(MKCHANGELOG "/usr/share/mkdeb/mkchangelog")
endif(NOT DEFINED MKCHANGELOG)
if(NOT DEFINED PACKAGE_VERSION)
    set(PACKAGE_VERSION 1)
endif(NOT DEFINED PACKAGE_VERSION)
set(FULL_VERSION ${PROJECT_VERSION}-${PACKAGE_VERSION})
set(DSC_FILE "${PROJECT_NAME}_${FULL_VERSION}.dsc")
if(NOT DEFINED DSC_CODENAME)
    execute_process(COMMAND lsb_release -cs OUTPUT_VARIABLE DSC_CODENAME)
endif(NOT DEFINED DSC_CODENAME)

add_custom_command(
    OUTPUT ${tarname}
    COMMAND git -C ${CMAKE_CURRENT_SOURCE_DIR} archive --prefix ${distname}/ HEAD | xz > ${CMAKE_CURRENT_BINARY_DIR}/${tarname}
    DEPENDS ${CMAKE_CURRENT_SOURCE_DIR}/.git/index
)
add_custom_target(dist DEPENDS "${tarname}")
add_custom_command(
    DEPENDS ${tarname}
    COMMAND rm -rf ${distname} && tar -xJf ${tarname}
    COMMAND "${MKCHANGELOG}" "${CMAKE_CURRENT_SOURCE_DIR}" "${distname}/debian" "${FULL_VERSION}" -c "${DSC_CODENAME}"
    COMMAND cd ${distname} && debuild -S
    OUTPUT "${DSC_FILE}"
)
add_custom_target(dsc DEPENDS "${DSC_FILE}")
add_custom_target(deb
    DEPENDS "${DSC_FILE}"
    COMMAND cd "${distname}" && dpkg-buildpackage --build=any,all
)
add_custom_target(debclean
    COMMAND rm -rf "${PROJECT_NAME}[-_]${PROJECT_VERSION}*"
)
